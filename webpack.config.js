const path = require('path');
const dotenv = require('dotenv-defaults');
const webpack = require('webpack');
const package = require('./package.json');
const dateFormat = require('dateformat');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const CompressionPlugin = require('compression-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const FaviconsWebpackPlugin = require('favicons-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const Terser = require('terser');
const BrotliPlugin = require('brotli-webpack-plugin');
const SitemapPlugin = require('sitemap-webpack-plugin').default;

module.exports = (env, opts) => {
    // webpack tool vars
    let devMode = opts.mode !== 'production';
    let hash = '[hash:6]'+ dateFormat(new Date(), 'ddmmyyHH');
    let fileEnv = dotenv.config({
        path: './.env',
        encoding: 'utf8',
        defaults: './.env.defaults'
    }).parsed;
    fileEnv['APP_NAME'] = package.name;
    fileEnv['APP_AUTHOR'] = package.author.name;
    let cssVars = '';
    let envKeys = Object.keys(fileEnv).reduce((prev, next) => {
        cssVars += `$${next}: ${JSON.stringify(fileEnv[next]).replace(/["]/g, '')}; `;
        prev[`env.${next}`] = JSON.stringify(fileEnv[next]);
        return prev;
    }, {});

    return {
        // Webpack configs
        plugins: [
            new webpack.DefinePlugin(envKeys),
            new CopyWebpackPlugin([
                {from: './src/_redirects', to: './'},
                {from: './src/netlify.toml', to: './'},
                {from: './src/robots.txt', to: './'},
                {from: './src/service-worker.js', to: './', transform: content => Terser.minify(content.toString()).code},
                {from: './src/assets/images/logo.png', to: './images/logo.png'}
            ]),
            new HtmlWebpackPlugin({
                template: path.join(__dirname, '/src/index.html'),
                filename: 'index.html',
                minify: {
                    collapseWhitespace: true
                },
                title: package.author.name +' - Web developer - '+ package.name.charAt(0).toUpperCase() + package.name.slice(1),
                meta: {
                    viewport: 'width=device-width, initial-scale=1.0, shrink-to-fit=no',
                    description: package.description,
                    keywords: package.keywords.toString(),
                    author: package.author.name,
                    copyright: package.author.name,
                    robots: 'index, follow',
                },
                url: package.author.url,
                scriptLoading: 'defer'
            }),
            new FaviconsWebpackPlugin({
                logo: './src/assets/images/logo.png',
                cache: false,
                mode: 'webapp',
                prefix: '/',
                inject: true,
                favicons: {
                    appName: package.name,
                    appDescription: package.description,
                    developerName: package.author.name,
                    developerURL: package.author.url,
                    version: package.version,
                    background: process.env.PRIMARY_COLOR,
                    theme_color: process.env.QUATERNARY_COLOR,
                    start_url: '/',
                    display: 'fullscreen',
                    scope: '/',
                    icons: {
                        android: true,
                        appleIcon: true,
                        appleStartup: false,
                        coast: false,
                        favicons: true,
                        firefox: true,
                        windows: true,
                        yandex: false
                    }
                }
            }),
            new MiniCssExtractPlugin({
                filename: devMode ? 'css/[name].min.css' : 'css/'+ hash +'.min.css'
            }),
            new CleanWebpackPlugin([
                'dist',
                'var/cache'
            ]),
            new CompressionPlugin({
                test: /\.js$|\.css$|\.html$|\.ttf$|\.otf$|\.woff$|\.eot$|\.txt$|\.pdf$|\.svg$/,
                algorithm: 'gzip',
                cache: devMode ? path.join(__dirname, '/var/cache/dev') : path.join(__dirname, '/var/cache/prod'),
                exclude: [/images/, /meta/]
            }),
            new BrotliPlugin({
                test: /\.js$|\.css$|\.html$|\.ttf$|\.otf$|\.woff$|\.eot$|\.txt$|\.pdf$|\.svg$/
            }),
            new SitemapPlugin(package.author.url, [
                '/',
                '/about',
                '/skills',
                '/contact'
            ])
        ],
        entry: ['babel-polyfill', path.join(__dirname, '/src/index.js')],
        output: {
            path: path.join(__dirname, '/dist'),
            filename: devMode ? 'js/[name].min.js' : 'js/'+ hash +'.min.js',
            publicPath: '/'
        },
        devServer: {
            host: process.env.SERVER_HOST,
            port: process.env.SERVER_PORT,
            contentBase: path.join(__dirname, '/dist'),
            compress: true,
            disableHostCheck: true,
            historyApiFallback: true,
            open: 'Chrome',
            headers: {
                'Cache-Control': 'max-age=31536000'
            }
        },
        resolve: {
            extensions: ['*', '.js', '.jsx'],
            symlinks: false,
            cacheWithContext: false
        },
        devtool: false,
        optimization: {
            minimizer: [
                new TerserPlugin({
                    test: /\.js(\?.*)?$/i,
                    cache: true,
                    parallel: true,
                    sourceMap: false
                }),
                new OptimizeCSSAssetsPlugin()
            ],
            splitChunks: {
                chunks: 'all',
            }
        },
        // Webpack modules
        module: {
            rules: [
                {
                    test: /\.js|jsx/,
                    exclude: /node_modules/,
                    loader: 'babel-loader',
                    query: {
                        presets:['@babel/env','@babel/react'],
                        plugins: [
                            ['react-intl', {
                                'messagesDir': './src/translations/'
                            }],
                            '@babel/plugin-proposal-class-properties'
                        ]
                    }
                },
                {
                    test: /\.(sa|sc|c)ss$/,
                    use: [
                        MiniCssExtractPlugin.loader,
                        {
                            loader: 'css-loader',
                            options: {
                                modules: false
                            }
                        },
                        {
                            loader: 'sass-loader',
                            options: {
                                data: cssVars
                            }
                        }
                    ]
                },
                {
                    test: /\.(png|jpe?g|gif|ico|svg|cur)$/,
                    exclude: [/fonts/],
                    use: [
                        {
                            loader: 'file-loader',
                            options: {
                                name: devMode ? 'images/[name].[ext]' : 'images/'+ hash +'.[ext]'
                            }
                        },
                        {
                            loader: 'image-webpack-loader',
                            options: {
                                mozjpeg: {
                                    progressive: true,
                                    quality: 65
                                },
                                pngquant: {
                                    quality: [0.65, 0.90],
                                    speed: 4
                                },
                                svgo: {
                                    plugins: [
                                        {
                                            removeViewBox: false
                                        },
                                        {
                                            removeEmptyAttrs: true
                                        }
                                    ]
                                }
                            }
                        }
                    ]
                },
                {
                    test: /\.(eot|ttf|otf|woff|woff2|svg)$/,
                    loader: 'file-loader',
                    exclude: [/images/],
                    options: {
                        name: devMode ? 'fonts/[name].[ext]' : 'fonts/'+ hash +'.[ext]'
                    }
                }
            ]
        }
    };
};
