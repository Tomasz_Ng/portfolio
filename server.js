require('dotenv').config();
const path = require('path');
const express = require('express');
const expressStaticGzip = require('express-static-gzip');
const app = express();

app.use(expressStaticGzip(path.join(__dirname, 'dist'), {
    enableBrotli: true,
    orderPreference: ['br'],
    serveStatic: {
        'setHeaders': setHeaders
    }
}));

function setHeaders (res, path) {
    res.setHeader('Accept-Encoding', 'gzip, br')
}

app.use(express.static(path.join(__dirname, '/dist'), {
    maxAge: '31557600'
}));

app.get('*', function (req, res, next) {
    if (req.originalUrl === '/error') {
        res.status(404);
    } else {
        res.status(200);
    }

    res.sendFile(path.join(__dirname, '/dist', 'index.html'));
});

app.listen(process.env.PORT || process.env.SERVER_PORT, function () {
    console.log('Web server is running');
});
