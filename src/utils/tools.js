export function handleSphere(sphere) {
  let context = sphere.getContext('2d');
  let width = sphere.width;
  let height = sphere.height;
  let rotation = 0;
  let dots = [];
  let dotsAmount = 250;
  let dotRadius = 4;
  let globeRadius = sphere.width * 0.7;
  let globeCenterZ = - sphere.width * 0.7;
  let projectionCenterX = sphere.width / 2;
  let projectionCenterY = sphere.height / 2;
  let fieldOfView = sphere.width * 0.8;

  class Dot {
    constructor(x, y, z, globeCenterZ, fieldOfView, projectionCenterX, projectionCenterY, dotRadius, context) {
      this.x = x;
      this.y = y;
      this.z = z;

      this.xProject = 0;
      this.yProject = 0;
      this.sizeProjection = 0;
      this.globeCenterZ = globeCenterZ;
      this.fieldOfView = fieldOfView;
      this.projectionCenterX = projectionCenterX;
      this.projectionCenterY = projectionCenterY;
      this.context = context;
      this.dotRadius = dotRadius;
    }

    project(sin, cos) {
      const rotX = cos * this.x + sin * (this.z - this.globeCenterZ);
      const rotZ = -sin * this.x + cos * (this.z - this.globeCenterZ) + this.globeCenterZ;
      this.sizeProjection = this.fieldOfView / (this.fieldOfView - rotZ);
      this.xProject = (rotX * this.sizeProjection) + this.projectionCenterX;
      this.yProject = (this.y * this.sizeProjection) + this.projectionCenterY;
    }

    draw(sin, cos) {
      this.project(sin, cos);
      //this.context.fillRect(this.xProject - this.dotRadius, this.yProject - this.dotRadius, this.dotRadius * 2 * this.sizeProjection, this.dotRadius * 2 * this.sizeProjection);
      this.context.beginPath();
      this.context.arc(this.xProject, this.yProject, this.dotRadius * this.sizeProjection, 0, Math.PI * 2);
      this.context.closePath();
      this.context.fillStyle = '#176660';
      this.context.fill();
    }
  }

  function render(a) {
    context.clearRect(0, 0, width, height);
    rotation = a * 0.0004;

    let sineRotation = Math.sin(rotation);
    let cosineRotation = Math.cos(rotation);

    for (let i = 0; i < dots.length; i++) {
      dots[i].draw(sineRotation, cosineRotation);
    }

    requestAnimationFrame(render);
  }

  function createDots() {
    dots.length = 0;

    for (let i = 0; i < dotsAmount; i++) {
      let theta = Math.random() * 2 * Math.PI;
      let phi = Math.acos((Math.random() * 2) - 1);
      let x = globeRadius * Math.sin(phi) * Math.cos(theta);
      let y = globeRadius * Math.sin(phi) * Math.sin(theta);
      let z = (globeRadius * Math.cos(phi)) + globeCenterZ;

      dots.push(new Dot(x, y, z, globeCenterZ, fieldOfView, projectionCenterX, projectionCenterY, dotRadius, context));
    }
  }

  createDots();
  requestAnimationFrame(render);
}
