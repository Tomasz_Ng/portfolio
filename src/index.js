import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-intl-redux';
import {ConnectedRouter} from 'connected-react-router';
import {store, browserHistory} from './store';
import App from './components/App';
import {handleSphere} from './utils/tools';
import './assets/styles/index.scss';
import ReactGA from 'react-ga';

ReactGA.initialize(env.GOOGLE_ANALYTICS_ID);

browserHistory.listen((location) => {
  ReactGA.pageview(location.pathname + location.search);
});
ReactDOM.render(
    <Provider store={store}>
      <ConnectedRouter history={browserHistory}>
        <App/>
      </ConnectedRouter>
    </Provider>,
    document.getElementById('root')
);

let sphere = document.getElementsByClassName('sphere')[0];
sphere.width = sphere.clientWidth;
sphere.height = sphere.clientHeight;

if ('serviceWorker' in navigator) {
  window.addEventListener('load', function() {
    navigator.serviceWorker.register('../service-worker.js')
      .then(function (registration) {
        console.log('Hooray. Service worker registered successfully, scope is:', registration.scope);

        if (window.Notification && Notification.permission !== 'granted') {
          Notification.requestPermission(function(result) {
            if (result !== 'granted') {
              console.log('No notification permission granted!');
            } else {
              console.log('Notification permission granted!');
              let messages = store.getState().intl.messages;

              navigator.serviceWorker.getRegistration().then(function(reg) {
                reg.showNotification(messages['notification_push.welcome.title'], {
                  body: messages['notification_push.welcome.body'],
                  icon: 'images/logo.png',
                  badge: 'images/logo.png'
                });
              });
            }
          });
        }

        if (sphere.transferControlToOffscreen) {
          let offscreen = sphere.transferControlToOffscreen();
          let worker = new Worker('service-worker.js');

          worker.postMessage({action: 'ANIMATE_CANVAS', canvas: offscreen}, [offscreen]);
          console.log('Canvas loaded from service worker!');
        } else {
          handleSphere(sphere);
        }
      }).catch(function (error) {
      console.log('Whoops. Service worker registration failed, error:', error);
    });

    navigator.serviceWorker.onmessage = async (event) => {
      switch (event.data.action) {
        case 'RELOAD':
          window.location.reload();
          break;
        default:
          break;
      }
    };

    window.unregisterServiceWorker = async function () {
      const cacheKeys = await caches.keys();

      for (const key of cacheKeys) {
        caches.delete(key)
      }

      const registrations = await navigator.serviceWorker.getRegistrations();

      for (const registration of registrations) {
        registration.postMessage({ action: 'UNREGISTER' })
      }
    };
  });
} else {
  handleSphere(sphere);
}
