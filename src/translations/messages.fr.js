export default {
    "app.language.fr": "Français",
    "app.language.en": "Anglais",
    "nav.home": "Accueil",
    "nav.about": "À propos",
    "nav.skills": "Compétences",
    "nav.contact": "Contact",
    "nav.error": "Erreur",
    "locale_switcher.title": "Choisir la langue",
    "home.full_stack": "Full-stack",
    "home.web_developer": "Web developer",
    "home.more.performance": "PERFormance",
    "home.more.performance_description": "architecture ROBUSTE",
    "home.more.good_practices": "BONNES pratiques",
    "home.more.good_practices_description": "NORMES w3c",
    "home.more.seo": "SEO",
    "home.more.seo_description": "référencement NATUREL",
    "home.more.accessibility": "ACCESsibilité",
    "home.more.accessibility_description": "support NAVIGATEURS et MOBILES",
    "home.tag.text": "codeVERT",
    "error.title": "ErReur 404",
    "error.content": "Oups, <br/> la page que vous recherchez <br/> n'existe pas ... <br/><br/> Veuillez vérifier l'URL.",
    "error.tag.text": "mauvaisePAGE",
    "notification_push.welcome.title": "Bonjour",
    "notification_push.welcome.body": "Bienvenue sur mon site web",
    "notification.welcome": "Bienvenue",
    "notification.welcome_back": "Ravis de vous revoir",
    "contact.discuss": "vous souhaitez discuter d'un PROJET ?",
    "contact.find_me": "Retrouvez moi sur :",
    "contact.tag.text": "disponible",
    "about.question_1": "QUI je suis ?",
    "about.answer_1": "Développeur web passioné.",
    "about.question_2": "ce que je FAIS",
    "about.answer_2": "Je me spécialise dans le développement <br/> de sites, applications et <br/> applications progressives [WEB].",
    "about.question_3": "ce que j'AIME",
    "about.answer_3": "Toucher à tout !",
    "about.more.title": "Ce projet a été réalisé avec",
    "about.more.subtitle": "Essayez le hors ligne !",
    "about.tag.text": "autodidacte",
    "skills.title_front": "front",
    "skills.title_back": "back",
    "skills.tag.text": "maitrise",
    "cue.title.bottom": "Défiler vers le bas",
    "cue.title.top": "Défiler vers le haut"
};
