import messagesEN from './messages.en.js';
import messagesFR from './messages.fr.js';

export default {
    'en': Object.assign(messagesEN),
    'fr': Object.assign(messagesFR)
};
