import Home from '../components/views/Home';
import About from '../components/views/About';
import Skills from '../components/views/Skills';
import Contact from '../components/views/Contact';

export default [
    {
        path: '/',
        name: 'home',
        exact: true,
        component: Home
    },
    {
        path: '/about',
        name: 'about',
        exact: true,
        component: About
    },
    {
        path: '/skills',
        name: 'skills',
        exact: true,
        component: Skills
    },
    {
        path: '/contact',
        name: 'contact',
        exact: true,
        component: Contact
    }
];
