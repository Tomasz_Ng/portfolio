import {applyMiddleware, compose, createStore} from 'redux';
import {routerMiddleware} from 'connected-react-router';
import {reducers, initialState} from '../reducers';
import {createBrowserHistory} from 'history';
import thunk from 'redux-thunk';
import {composeWithDevTools} from 'redux-devtools-extension';

export const browserHistory = createBrowserHistory();
const middlewares = [thunk, routerMiddleware(browserHistory)];
const enhancer = env.APP_ENV.indexOf('dev') > -1 ? composeWithDevTools : compose;

export const store = createStore(
  reducers(browserHistory),
  initialState,
  enhancer(
    applyMiddleware(
      ...middlewares
    )
  )
);
