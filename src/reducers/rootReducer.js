import routes from '../config/routes';
import messages from '../translations';

const navigatorLanguage = navigator.language.substring(0, 2);

export const initialState = {
  root: {
    isIE: false || !!document.documentMode,
    isFirefox: navigator.userAgent.toLowerCase().indexOf('firefox') > -1,
    routes: routes,
    locales: env.APP_LOCALES.split('|'),
    route: 'home',
    changingRoute: '',
    isMobilePortrait: window.innerWidth < env.MOBILE_BREAKPOINT.replace('px', ''),
    isMobileLandscape: window.innerWidth >= env.MOBILE_BREAKPOINT.replace('px', '') && window.innerWidth < env.TABLET_SM_BREAKPOINT.replace('px', ''),
    isTabletPortrait: window.innerWidth >= env.TABLET_SM_BREAKPOINT.replace('px', '') && window.innerWidth < env.TABLET_XL_BREAKPOINT.replace('px', ''),
    isTabletLandscape: window.innerWidth >= env.TABLET_XL_BREAKPOINT.replace('px', '') && window.innerWidth < env.DESKTOP_BREAKPOINT.replace('px', ''),
    isDesktop: window.innerWidth >= env.DESKTOP_BREAKPOINT.replace('px', ''),
    isElementsAnimationLoaded: false,
    isLocaleSwitcherOpen: false,
    isLocaleSwitch: false,
    isFirstRendering: true,
    isScrollingTop: false,
    isScrollingBottom: false,
    anagram: sessionStorage.getItem('anagram') ? sessionStorage.getItem('anagram') : 'freelance',
    content_author: sessionStorage.getItem('author') ? sessionStorage.getItem('author') : 'tomAsz ngoNdo',
    content_specialty: sessionStorage.getItem('specialty') ? sessionStorage.getItem('specialty') : 'FulL-staCk',
    content_job: sessionStorage.getItem('job') ? sessionStorage.getItem('job') : 'wEb dEvelopER',
    content_error_title: sessionStorage.getItem('error-title') ? sessionStorage.getItem('error-title') : messages[localStorage.getItem('locale') ? localStorage.getItem('locale') : navigatorLanguage]['error.title'],
    mainNav: getMainNav(location.pathname, localStorage.getItem('locale') ? localStorage.getItem('locale') : navigatorLanguage, window.innerWidth < env.MOBILE_BREAKPOINT.replace('px', '')),
    sphereCanvas: document.getElementsByClassName('sphere')[0],
    body: document.getElementsByTagName('body')[0],
    html: document.getElementsByTagName('html')[0],
    sphereMaxSize: (window.innerHeight / 4) * 3,
    sphereMinSize: window.innerHeight / 4,
    is404: false
  },
  intl: {
    locale: localStorage.getItem('locale') ? localStorage.getItem('locale') : navigatorLanguage,
    defaultLocale: localStorage.getItem('locale') ? localStorage.getItem('locale') : navigatorLanguage,
    messages: messages[localStorage.getItem('locale') ? localStorage.getItem('locale') : navigatorLanguage],
  }
};

export const rootReducer = (state = initialState, action) => {
  function enlargeSphere(timestamp) {
    state.sphereMinSize = state.sphereMinSize + 32;

    state.sphereCanvas.style.width = state.sphereMinSize + 'px';
    state.sphereCanvas.style.height = state.sphereMinSize + 'px';

    if (state.sphereMinSize < state.sphereMaxSize) {
      requestAnimationFrame(enlargeSphere);
    }
  }

  function shrinkSphere(timestamp) {
    state.sphereMaxSize = state.sphereMaxSize - 32;

    state.sphereCanvas.style.width = state.sphereMaxSize + 'px';
    state.sphereCanvas.style.height = state.sphereMaxSize + 'px';

    if (state.sphereMinSize < state.sphereMaxSize) {
      requestAnimationFrame(shrinkSphere);
    }
  }

  switch (action.type) {
    case '@@INIT':
      return state;
    case '@@router/LOCATION_CHANGE':
      state.changingRoute = getRouteName(action.payload.location.pathname);
      document.getElementById('root').dataset.route = state.changingRoute;
      state.isFirstRendering = action.payload.isFirstRendering;
      state.mainNav = getMainNav(action.payload.location.pathname, localStorage.getItem('locale') ? localStorage.getItem('locale') : navigatorLanguage, state.isMobilePortrait);
      state.isElementsAnimationLoaded = true;

      if (!sessionStorage.getItem('first-action') && action.payload.action === 'PUSH') {
        sessionStorage.setItem('first-action', 'true');
      }

      if (!localStorage.getItem('anonymous')) {
        localStorage.setItem('anonymous', 'true');
      }

      localStorage.getItem('locale') ? state.html.setAttribute('lang', localStorage.getItem('locale')) : state.html.setAttribute('lang', navigatorLanguage);

      if (state.isLocaleSwitch && action.payload.action === 'PUSH' && state.isFirstRendering) {
        state.isLocaleSwitch = true;
      } else if (!state.isLocaleSwitch && action.payload.action === 'PUSH'){
        state.isLocaleSwitch = false;
      } else if (state.isLocaleSwitch && action.payload.action === 'PUSH' && !state.isFirstRendering) {
        state.isLocaleSwitch = false;
      }

      if (state.isFirstRendering) {
        state.isScrollingTop = true;
      } else {
        state.isScrollingTop = true;
        state.isScrollingBottom = false;
      }

      switch (state.changingRoute) {
        case 'home':
          if (state.route === 'error') {
            location.pathname = '/';
          }
          state.route = state.changingRoute;
          return state;
        case 'skills':
          animateValue('percent-front', 0, 90, 3000);
          animateValue('percent-back', 0, 70, 2500);
          if (state.route === 'error') {
            location.pathname = state.changingRoute;
          }
          state.route = state.changingRoute;
          return state;
        case 'error':
          if (location.pathname !== '/error') {
            location.pathname = '/error';
          }
          state.route = state.changingRoute;
          return state;
        default:
          if (state.route === 'error') {
            location.pathname = state.changingRoute;
          }
          state.route = state.changingRoute;
          return state;
      }
    case '@@intl/UPDATE':
      state.html.setAttribute('lang', action.payload.locale);
      localStorage.setItem('locale', action.payload.locale);

      return {
        ...state,
        isLocaleSwitch: true,
        isLocaleSwitcherOpen: !state.isLocaleSwitcherOpen,
        isScrollingTop: true,
        isScrollingBottom: false
      };
    case 'TOGGLE_LOCALE_SWITCHER':
      return {
        ...state,
        isLocaleSwitcherOpen: action.isLocaleSwitcherOpen
      };
    case 'HANDLE_ANAGRAM':
      let elementsLetters = document.getElementsByClassName('sub-element letter');
      let targetLetterUpper = action.targetLetter.toUpperCase();
      let targetLetterLower = action.targetLetter.toLowerCase();
      let i = 0;

      action.letter.innerHTML = action.targetLetter === targetLetterUpper ? targetLetterLower : targetLetterUpper;

      if (state.anagram.length < 9) {
        if (action.letter.textContent === action.letter.textContent.toUpperCase()) {
          state.anagram += action.targetLetter;
        } else {
          state.anagram = state.anagram.indexOf(targetLetterLower) > -1 ? state.anagram.replace(targetLetterLower, '') : state.anagram;
        }
      } else {
        state.anagram = action.targetLetter;

        for (i = 0; i < elementsLetters.length; i++) {
          elementsLetters[i].innerHTML = elementsLetters[i].textContent.toLowerCase();
        }

        action.letter.innerHTML = action.targetLetter === targetLetterUpper ? targetLetterLower : targetLetterUpper;
      }

      sessionStorage.setItem('anagram', state.anagram);

      let authorElement = document.getElementsByClassName('author')[0];
      let specialtyElement = document.getElementsByClassName('specialty')[0];
      let jobElement = document.getElementsByClassName('job')[0];
      let errorTitleElement = document.getElementsByClassName('error-title')[0];
      let contentAuthor = authorElement ? authorElement.textContent : state.content_author;
      let contentSpecialty = specialtyElement ? specialtyElement.textContent : state.content_specialty;
      let contentJob = jobElement ? jobElement.textContent : state.content_job;
      let contentErrorTitle = errorTitleElement ? errorTitleElement.textContent : state.content_error_title;

      authorElement ? sessionStorage.setItem('author', contentAuthor) : null;
      specialtyElement ? sessionStorage.setItem('specialty', contentSpecialty) : null;
      jobElement ? sessionStorage.setItem('job', contentJob) : null;
      errorTitleElement ? sessionStorage.setItem('error-title', contentErrorTitle) : null;

      return {
        ...state,
        content_author: contentAuthor,
        content_specialty: contentSpecialty,
        content_job: contentJob,
        content_error_title: contentErrorTitle
      };
    case 'SCROLL_TOP':
      if (state.route === 'about') {
        state.body.style.backgroundColor = env.PRIMARY_COLOR;
      }

      document.getElementsByClassName('tag')[0].style.opacity = 1;
      document.getElementsByClassName('anagram')[0].style.opacity = 1;

      if (state.sphereCanvas.clientWidth < state.sphereMinSize) {
        requestAnimationFrame(enlargeSphere);
      }

      return {
        ...state,
        isScrollingTop: true,
        isScrollingBottom: false
      };
    case 'SCROLL_BOTTOM':
      if (state.route === 'about') {
        state.body.style.backgroundColor = 'rgba(0,0,0,1)';
      }

      if (state.route !== 'skills') {
        document.getElementsByClassName('tag')[0].style.opacity = 0;
        document.getElementsByClassName('anagram')[0].style.opacity = 0;
      }

      if ((state.isMobilePortrait ? state.sphereCanvas.clientWidth + 200 : state.sphereCanvas.clientWidth + 20 > state.sphereMaxSize) && !state.isScrollingBottom) {
        requestAnimationFrame(shrinkSphere);
      }

      return {
        ...state,
        isScrollingTop: false,
        isScrollingBottom: true
      };
    default:
      return state;
  }
};

function getRouteName(pathname) {
  let route = initialState.root.routes.filter(route => {
    return route.path === pathname;
  });

  return route.length !== 0 ? route[0].name : 'error';
}

function getMainNav(pathname, locale, isMobilePortrait) {
  console.log(isMobilePortrait)
  return {
    home: {
      to: '/',
      className: 'home-link nav-element element element-1',
      title: messages[locale ? locale : navigatorLanguage]['nav.home'],
      x: '3',
      y: pathname === '/skills' ? '-3' : '0',
      scale: '1',
      rotateMin: '30',
      rotateMax: '-18',
      width: '6',
      height: '6',
      fontSize: '2',
      duration: '450',
      borderRadius: '50',
      padding: '5',
      elemTextDuration: '650',
      elemTextClassName: 'sub-element icon icon-home'
    },
    about: {
      to: '/about',
      className: 'about-link nav-element element ' + (pathname === '/contact' || pathname === '/skills' ? 'element-2' : 'element-1'),
      title: messages[locale ? locale : navigatorLanguage]['nav.about'],
      x: pathname === '/contact' ? '85' : pathname === '/skills' ? '45' : '3',
      y: pathname === '/contact' ? '-7' : pathname === '/skills' && isMobilePortrait ? '30' : pathname === '/skills' && !isMobilePortrait ? '33' : '-3',
      scale: '1',
      rotateMin: pathname === '/contact' ? '0' : pathname === '/skills' ? '0' : '20',
      rotateMax: pathname === '/contact' ? '20' : pathname === '/skills' ? '0' : '-30',
      width: '6',
      height: '6',
      fontSize: '2',
      duration: '450',
      borderRadius: '50',
      padding: '5',
      elemTextDuration: '650',
      elemTextClassName: 'sub-element icon icon-about'
    },
    contact: {
      to: '/contact',
      className: 'contact-link nav-element element ' + (pathname === '/skills' ? 'element-3' : 'element-2'),
      title: messages[locale ? locale : navigatorLanguage]['nav.contact'],
      x: '85',
      y: pathname === '/skills' ? '-18' : pathname === '/about' ? '-7' : '-10',
      scale: '1',
      rotateMin: pathname === '/' ? '-25' : pathname === '/about' ? '20' : '-20',
      rotateMax: pathname === '/' ? '35' : pathname === '/about' ? '-5' : '40',
      width: '6',
      height: '6',
      fontSize: '2',
      duration: '450',
      borderRadius: '50',
      padding: '5',
      elemTextDuration: '650',
      elemTextClassName: 'sub-element icon icon-contact'
    },
    skills: {
      to: '/skills',
      className: 'skills-link nav-element element element-3',
      title: messages[locale ? locale : navigatorLanguage]['nav.skills'],
      x: pathname === '/' ? '45' : pathname === '/about' ? '48' : '46',
      y: pathname === '/' && isMobilePortrait ? '20' : pathname === '/' && !isMobilePortrait ? '23' : pathname === '/about' && isMobilePortrait ? '30' : pathname === '/about' && !isMobilePortrait ? '33' : isMobilePortrait ? '25' : '28',
      scale: '1',
      rotateMin: pathname === '/' ? '15' : pathname === '/about' ? '20' : '-5',
      rotateMax: pathname === '/' ? '15' : pathname === '/about' ? '-5' : '20',
      width: '6',
      height: '6',
      fontSize: '2',
      duration: '450',
      borderRadius: '50',
      padding: '5',
      elemTextDuration: '650',
      elemTextClassName: 'sub-element icon icon-skills'
    }
  }
}

function animateValue (classname, start, end, duration) {
  let range = end - start;
  let current = start;
  let increment = end > start ? 1 : -1;
  let stepTime = Math.abs(Math.floor(duration / range));
  let obj = document.getElementsByClassName(classname);
  let timer = setInterval(function() {
    current += increment;
    obj[0] ? obj[0].innerHTML = current + '%' : null;

    if (current === end) {
      clearInterval(timer);
    }
  }, stepTime);
}
