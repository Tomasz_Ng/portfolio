import {combineReducers} from 'redux';
import {connectRouter} from 'connected-react-router';
import {intlReducer} from 'react-intl-redux';
import {rootReducer, initialState as rootState} from './rootReducer';

export const initialState = Object.assign(rootState);

export const reducers = (browserHistory) => combineReducers({
    'root': rootReducer,
    'intl': intlReducer,
    'router': connectRouter(browserHistory)
});
