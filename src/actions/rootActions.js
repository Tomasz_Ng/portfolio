import {updateIntl} from 'react-intl-redux';
import messages from '../translations';
import anime from 'animejs';
import {
  TOGGLE_LOCALE_SWITCHER,
  HANDLE_ANAGRAM,
  SCROLL_TOP,
  SCROLL_BOTTOM
} from '../constants/action-types';

export const changeLocale = ({dispatch}) => {
  return (nextLocale) => {
    dispatch(updateIntl({
      locale: nextLocale,
      messages: messages[nextLocale]
    }));
  };
};

export const animateElements = () => {
  return () => {
    let subElements = document.getElementsByClassName('sub-element');
    let i = 0;
    let elementsAnimation = anime({
      targets: '.element',
      left: function(el) {
        return el.getAttribute('data-x') + '%';
      },
      translateY: function(el, i) {
        return el.getAttribute('data-y') + 'vh';
      },
      scale: function(el, i, l) {
        return el.getAttribute('data-scale');
      },
      rotate: function(el, i) {
        return anime.random(-el.getAttribute('data-rotate-min'), el.getAttribute('data-rotate-max'));
      },
      width: function(el, i) {
        return el.getAttribute('data-width') + 'vh';
      },
      height: function(el, i) {
        return el.getAttribute('data-height') + 'vh';
      },
      fontSize: function(el) {
        return el.getAttribute('data-font-size') + 'vh';
      },
      duration: function(el) {
        return el.getAttribute('data-duration');
      },
      borderRadius: function(el) {
        return el.getAttribute('data-border-radius') + 'px';
      },
      padding: function(el) {
        return el.getAttribute('data-padding') + 'px';
      },
      boxShadow: '3px 3px 3px -1px rgba(0,0,0,0.50)',
      opacity: 1,
      easing: 'easeInOutBack',
      autoplay: true,
      loop: false,
      changeBegin: function(anim) {
        for (i = 0; i < subElements.length; i++) {
          subElements[i].style.opacity = 1;
        }
      }
    });

    elementsAnimation.play();
  };
};

export const toggleLocaleSwitcher = ({dispatch}) => {
  return (isLocaleSwitcherOpen) => {
    dispatch({
      type: TOGGLE_LOCALE_SWITCHER,
      isLocaleSwitcherOpen: isLocaleSwitcherOpen
    });
  };
};

export const animateNotification = () => {
  return (callback) => {
    anime.timeline({
      loop: false,
      delay: anime.stagger(50, {start: 0}),
      changeComplete: function() {
        callback();
      }
    })
    .add({
      targets: '.notification .rounded-container',
      scale: [0, 1],
      duration: 250,
      easing: 'easeInOutExpo'
    })
    .add({
      targets: '.notification .rounded-plain',
      scale: [0, 1],
      duration: 200,
      easing: 'easeOutExpo',
      offset: '-=400'
    })
    .add({
      targets: '.notification .text-left',
      scale: [0, 1],
      duration: 200,
      offset: '-=400'
    })
    .add({
      targets: '.notification',
      opacity: 0,
      duration: 200,
      easing: 'easeOutExpo',
      delay: 50
    });
  }
};

export const handleAnagram = ({dispatch}) => {
  return (event) => {
    dispatch({
      type: HANDLE_ANAGRAM,
      targetLetter: event.currentTarget.textContent,
      letter: event.currentTarget
    });
  };
};

export const handleScroll = ({dispatch}) => {
  return (event = null, direction = null) => {
    let scrollDirection = '';

    if (event) {
      event.preventDefault();
      event.stopPropagation();

      scrollDirection = Math.max(-1, Math.min(1, (event.wheelDelta || -event.detail)));
    } else {
      scrollDirection = direction;
    }

    if ((scrollDirection > 0 || (event && event.dir === 'Down'))) {
      dispatch({
        type: SCROLL_TOP
      });
    } else if ((scrollDirection < 0 || (event && event.dir === 'Up'))) {
      dispatch({
        type: SCROLL_BOTTOM
      });
    }
  };
};

export const handleSwipe = ({dispatch}) => {
  return (event, offset) => {
    event.preventDefault();
    event.stopPropagation();

    if (offset.y < 0) {
      dispatch({
        type: SCROLL_TOP
      });
    } else if (offset.y > 0) {
      dispatch({
        type: SCROLL_BOTTOM
      });
    }
  };
};
