import React from 'react';
import {connect} from 'react-redux';

const mapStateToProps = (state) => {
  return {
    messages: state.intl.messages,
    route: state.root.route,
    isScrollingBottom: state.root.isScrollingBottom
  };
};

const Tag = (props) => {
  const message = props.route === 'about' ? props.messages['about.tag.text'] : props.route === 'skills' ? props.messages['skills.tag.text'] : props.route === 'contact' ? props.messages['contact.tag.text'] : props.route === 'home' ? props.messages['home.tag.text'] : props.messages['error.tag.text'];

  return (
    <h2 className="tag">
      {message}
    </h2>
  );
};

export default connect(mapStateToProps, null)(Tag);
