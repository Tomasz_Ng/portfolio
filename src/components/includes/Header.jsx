import React from 'react';
import {Link, withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import LocaleSwitcher from '../widgets/LocaleSwitcher';
import {
  HeaderNavPosedUl,
  HeaderNavPosedLi
} from '../core/Pose';

const mapStateToProps = (state) => {
  return {
    messages: state.intl.messages,
    isScrollingBottom: state.root.isScrollingBottom,
    route: state.root.route
  };
};

const Header = (props) => {
  return (
    <header>
      <nav className="header-nav">
        <Link
          to="/"
          className="home-link icon icon-logo"
          title={props.messages['nav.home']}
          onClick={(event) => props.route === 'home' && event.preventDefault()}>
        </Link>

        <HeaderNavPosedUl pose={props.isScrollingBottom ? 'show' : 'hide'}>
          {props.route !== 'about' ? <HeaderNavPosedLi>
            <Link
              to="/about"
              className="about-link icon icon-about"
              title={props.messages['nav.about']}>
            </Link>
          </HeaderNavPosedLi> : null}

          {props.route !== 'skills' ? <HeaderNavPosedLi>
            <Link
              to="/skills"
              className="skills-link icon icon-skills"
              title={props.messages['nav.skills']}>
            </Link>
          </HeaderNavPosedLi> : null}

          {props.route !== 'contact' ? <HeaderNavPosedLi>
            <Link
              to="/contact"
              className="contact-link icon icon-contact"
              title={props.messages['nav.contact']}>
            </Link>
          </HeaderNavPosedLi> : null}
        </HeaderNavPosedUl>
      </nav>

      <LocaleSwitcher/>
    </header>
  );
};

export default withRouter(connect(mapStateToProps, null)(Header));
