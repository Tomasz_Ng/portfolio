import React, {useEffect, useState} from 'react';
import {Link} from 'react-router-dom';
import {animated} from 'react-spring/web.cjs';
import {connect} from 'react-redux';
import ReactGA from "react-ga";

const mapStateToProps = (state) => {
  return {
    route: state.root.route,
    mainNav: Object.entries(state.root.mainNav),
    isDesktop: state.root.isDesktop
  };
};

const Nav = (props) => {
  const [isMouseMoving, setIsMouseMoving] = useState(true);

  const mouseMoveHandler = (event) => {
    return isMouseMoving ? props.set({ xy: props.calc(event.clientX, event.clientY)}) : props.stop ? props.stop() : null;
  };

  useEffect(() => {
    let mainSection = document.getElementsByClassName('main-section')[0];
    let links = document.getElementsByClassName('nav-element');

    props.isDesktop ? mainSection.addEventListener('mouseover', mouseMoveHandler, {passive: true}) : null;

    for (let i = 0; i < links.length; i++) {
      links[i].onclick = (event) => {
        setIsMouseMoving(false);
      };
    }
  }, [props.route]);

  return (
    <animated.nav className="nav" style={{transform: props.positions.xy.to(props.trans1)}}>
      {props.mainNav.map((navLink, i) => {
        switch(props.route) {
          case 'home':
            if (navLink[0] !== 'home') {
              return <Link
                key={i}
                to={navLink[1].to}
                className={navLink[1].className}
                title={navLink[1].title}
                data-x={navLink[1].x}
                data-y={navLink[1].y}
                data-scale={navLink[1].scale}
                data-rotate-min={navLink[1].rotateMin}
                data-rotate-max={navLink[1].rotateMax}
                data-width={navLink[1].width}
                data-height={navLink[1].height}
                data-font-size={navLink[1].fontSize}
                data-duration={navLink[1].duration}
                data-border-radius={navLink[1].borderRadius}
                data-padding={navLink[1].padding}
                onClick={() => {
                  ReactGA.event({
                    category: 'Nav link',
                    action: 'click on link',
                    value: 1
                  });
                }}>

                <span
                  className={navLink[1].elemTextClassName}
                  data-duration={navLink[1].elemTextDuration}>
                </span>
              </Link>;
            }
            break;
          case 'about':
            if (navLink[0] !== 'about') {
              return <Link
                key={i}
                to={navLink[1].to}
                className={navLink[1].className}
                title={navLink[1].title}
                data-x={navLink[1].x}
                data-y={navLink[1].y}
                data-scale={navLink[1].scale}
                data-rotate-min={navLink[1].rotateMin}
                data-rotate-max={navLink[1].rotateMax}
                data-width={navLink[1].width}
                data-height={navLink[1].height}
                data-font-size={navLink[1].fontSize}
                data-duration={navLink[1].duration}
                data-border-radius={navLink[1].borderRadius}
                data-padding={navLink[1].padding}
                onClick={() => {
                  ReactGA.event({
                    category: 'Nav link',
                    action: 'click on link',
                    value: 1
                  });
                }}>

                <span
                  className={navLink[1].elemTextClassName}
                  data-duration={navLink[1].elemTextDuration}>
                </span>
              </Link>;
            }
            break;
          case 'skills':
            if (navLink[0] !== 'skills') {
              return <Link
                key={i}
                to={navLink[1].to}
                className={navLink[1].className}
                title={navLink[1].title}
                data-x={navLink[1].x}
                data-y={navLink[1].y}
                data-scale={navLink[1].scale}
                data-rotate-min={navLink[1].rotateMin}
                data-rotate-max={navLink[1].rotateMax}
                data-width={navLink[1].width}
                data-height={navLink[1].height}
                data-font-size={navLink[1].fontSize}
                data-duration={navLink[1].duration}
                data-border-radius={navLink[1].borderRadius}
                data-padding={navLink[1].padding}
                onClick={() => {
                  ReactGA.event({
                    category: 'Nav link',
                    action: 'click on link',
                    value: 1
                  });
                }}>

                <span
                  className={navLink[1].elemTextClassName}
                  data-duration={navLink[1].elemTextDuration}>
                </span>
              </Link>;
            }
            break;
          case 'contact':
            if (navLink[0] !== 'contact') {
              return <Link
                key={i}
                to={navLink[1].to}
                className={navLink[1].className}
                title={navLink[1].title}
                data-x={navLink[1].x}
                data-y={navLink[1].y}
                data-scale={navLink[1].scale}
                data-rotate-min={navLink[1].rotateMin}
                data-rotate-max={navLink[1].rotateMax}
                data-width={navLink[1].width}
                data-height={navLink[1].height}
                data-font-size={navLink[1].fontSize}
                data-duration={navLink[1].duration}
                data-border-radius={navLink[1].borderRadius}
                data-padding={navLink[1].padding}
                onClick={() => {
                  ReactGA.event({
                    category: 'Nav link',
                    action: 'click on link',
                    value: 1
                  });
                }}>

                <span
                  className={navLink[1].elemTextClassName}
                  data-duration={navLink[1].elemTextDuration}>
                </span>
              </Link>;
            }
            return;
          default:
            if (navLink[0] !== 'home') {
              return <Link
                key={i}
                to={navLink[1].to}
                className={navLink[1].className}
                title={navLink[1].title}
                data-x={navLink[1].x}
                data-y={navLink[1].y}
                data-scale={navLink[1].scale}
                data-rotate-min={navLink[1].rotateMin}
                data-rotate-max={navLink[1].rotateMax}
                data-width={navLink[1].width}
                data-height={navLink[1].height}
                data-font-size={navLink[1].fontSize}
                data-duration={navLink[1].duration}
                data-border-radius={navLink[1].borderRadius}
                data-padding={navLink[1].padding}
                onClick={() => {
                  ReactGA.event({
                    category: 'Nav link',
                    action: 'click on link',
                    value: 1
                  });
                }}>

                <span
                  className={navLink[1].elemTextClassName}
                  data-duration={navLink[1].elemTextDuration}>
                </span>
              </Link>;
            }
            return;
        }
      })}
    </animated.nav>
  );
};

export default connect(mapStateToProps, null)(Nav);
