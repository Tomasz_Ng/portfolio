import React from 'react';

const Footer = () => {
  return (
    <footer>
      <p>Copyright ©</p> <time>&nbsp;{new Date().getFullYear()}&nbsp;</time> <h1>{env.APP_AUTHOR}</h1>
    </footer>
  );
};

export default Footer;
