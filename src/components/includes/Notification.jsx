import React from 'react';
import {connect} from 'react-redux';

const mapStateToProps = (state) => {
  return {
    isLocaleSwitch: state.root.isLocaleSwitch,
    messages: state.intl.messages,
    locale: state.intl.locale,
    route: state.root.route
  };
};

const Notification = (props) => {
  return (
    <aside className="notification">
      <h3 className="notification-text text-left">
        {props.isLocaleSwitch ? props.messages['app.language.'+ props.locale] : sessionStorage.getItem('first-action') ? props.messages['nav.'+ props.route] : localStorage.getItem('anonymous') ? props.messages['notification.welcome_back'] : props.messages['notification.welcome']}
      </h3>

      <div className="rounded rounded-plain"></div>

      <div className="rounded rounded-container">
        <div className="rounded rounded-dashed"></div>
      </div>
    </aside>
  );
};

export default connect(mapStateToProps, null)(Notification);
