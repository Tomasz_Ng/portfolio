import React, {Fragment} from 'react';
import {connect} from 'react-redux';
import {animated} from 'react-spring/web.cjs';
import {
  MorePosedSection,
  SkillMorePosedP,
  AboutMorePosedP,
  AboutMorePosedSpan,
  HomeMorePosedAside
} from '../core/Pose';

const mapStateToProps = (state) => {
  return {
    route: state.root.route,
    isScrollingBottom: state.root.isScrollingBottom,
    messages: state.intl.messages,
    isMobilePortrait: state.root.isMobilePortrait
  };
};

const More = (props) => {
  const handleDragStart = () => {
    let root = document.getElementById('root');
    root.removeEventListener('touchmove', props.touchMoveHandler, true);
  };

  const handleDragEnd = () => {
    let root = document.getElementById('root');
    root.addEventListener('touchmove', props.touchMoveHandler, true);
  };

  const handleContent = () => {
    switch (props.route) {
      case 'home':
        return <Fragment>
          <aside>
            <h3 className="title">{props.messages['home.more.performance']}</h3>
            <div className="sub-title">
              <p>{props.messages['home.more.performance_description']}</p>
              <span className='tree icon icon-tree'></span>
            </div>
          </aside>

          <aside>
            <h3 className="title">{props.messages['home.more.accessibility']}</h3>
            <div className="sub-title">
              <p>{props.messages['home.more.accessibility_description']}</p>

              <ul className="browsers">
                <li className="explorer icon icon-explorer"></li>
                <li className="safari icon icon-safari"></li>
                <li className="opera icon icon-opera"></li>
                <li className="edge icon icon-edge"></li>
                <li className="chrome icon icon-chrome"></li>
                <li className="firefox icon icon-firefox"></li>
              </ul>
            </div>
          </aside>

          <aside>
            <h3 className="title">{props.messages['home.more.seo']}</h3>
            <div className="sub-title">
              {props.messages['home.more.seo_description']}
              <span className='embed icon icon-embed'></span>
            </div>
          </aside>

          <aside>
            <h3 className="title">{props.messages['home.more.good_practices']}</h3>
            <div className="sub-title">
              {props.messages['home.more.good_practices_description']}
              <span className='clipboard icon icon-clipboard'></span>
            </div>
          </aside>
        </Fragment>;
      case 'about':
        return <Fragment>
          <AboutMorePosedP className="title">{props.messages['about.more.title']}</AboutMorePosedP>
          <AboutMorePosedSpan className="react-logo icon icon-react" title="ReactJS" positionX={props.isMobilePortrait ? window.innerWidth / 16 : window.innerWidth / 8}></AboutMorePosedSpan>
          <AboutMorePosedSpan className="webpack-logo icon icon-webpack" title="Webpack" positionX={-(props.isMobilePortrait ? window.innerWidth / 16 : window.innerWidth / 8)}><span className="path1"></span><span className="path2"></span></AboutMorePosedSpan>
          <AboutMorePosedP className="sub-title">{props.messages['about.more.subtitle']}</AboutMorePosedP>
        </Fragment>;
      case 'skills':
        return <Fragment>
          <animated.article className="back-skills" style={{transform: props.positions.xy.to(props.trans2)}}>
            <SkillMorePosedP
              className="skill symfony-skill"
              scale={1.1}
              onDragStart={() => handleDragStart()}
              onDragEnd={() => handleDragEnd()}>
                <strong className="title">Symfony</strong>
                <span className="percent">75%</span>
            </SkillMorePosedP>
            <SkillMorePosedP
              className="skill php-skill"
              scale={1.5}
              onDragStart={() => handleDragStart()}
              onDragEnd={() => handleDragEnd()}>
                <strong className="title">PHP</strong>
                <span className="percent">68%</span>
            </SkillMorePosedP>
            <SkillMorePosedP
              className="skill docker-skill"
              scale={0.8}
              onDragStart={() => handleDragStart()}
              onDragEnd={() => handleDragEnd()}>
                <strong className="title">Docker</strong>
                <span className="percent">60%</span>
            </SkillMorePosedP>
            <SkillMorePosedP
              className="skill node-skill"
              scale={0.8}
              onDragStart={() => handleDragStart()}
              onDragEnd={() => handleDragEnd()}>
                <strong className="title">NodeJS</strong>
                <span className="percent">60%</span>
            </SkillMorePosedP>
            <SkillMorePosedP
              className="skill jenkins-skill"
              scale={0.8}
              onDragStart={() => handleDragStart()}
              onDragEnd={() => handleDragEnd()}>
                <strong className="title">Jenkins</strong>
                <span className="percent">55%</span>
            </SkillMorePosedP>
            <SkillMorePosedP
              className="skill mongo-skill"
              scale={0.8}
              onDragStart={() => handleDragStart()}
              onDragEnd={() => handleDragEnd()}>
                <strong className="title">MongoDB</strong>
                <span className="percent">65%</span>
            </SkillMorePosedP>
            <SkillMorePosedP
              className="skill mysql-skill"
              scale={0.8}
              onDragStart={() => handleDragStart()}
              onDragEnd={() => handleDragEnd()}>
                <strong className="title">MySQL</strong>
                <span className="percent">68%</span>
            </SkillMorePosedP>
          </animated.article>

          <animated.article className="front-skills" style={{transform: props.positions.xy.to(props.trans1)}}>
            <SkillMorePosedP
              className="skill react-skill"
              scale={1.3}
              onDragStart={() => handleDragStart()}
              onDragEnd={() => handleDragEnd()}>
                <strong className="title">ReactJS</strong>
                <span className="percent">85%</span>
            </SkillMorePosedP>
            <SkillMorePosedP
              className="skill html-skill"
              scale={2}
              onDragStart={() => handleDragStart()}
              onDragEnd={() => handleDragEnd()}>
                <strong className="title">HTML</strong>
                <span className="percent">95%</span>
            </SkillMorePosedP>
            <SkillMorePosedP
              className="skill css-skill"
              scale={2}
              onDragStart={() => handleDragStart()}
              onDragEnd={() => handleDragEnd()}>
                <strong className="title">CSS</strong>
                <span className="percent">91%</span>
            </SkillMorePosedP>
            <SkillMorePosedP
              className="skill js-skill"
              scale={1}
              onDragStart={() => handleDragStart()}
              onDragEnd={() => handleDragEnd()}>
                <strong className="title">Javascript</strong>
                <span className="percent">82%</span>
            </SkillMorePosedP>
            <SkillMorePosedP
              className="skill redux-skill"
              scale={1}
              onDragStart={() => handleDragStart()}
              onDragEnd={() => handleDragEnd()}>
                <strong className="title">Redux</strong>
                <span className="percent">75%</span>
            </SkillMorePosedP>
            <SkillMorePosedP
              className="skill sass-skill"
              scale={1}
              onDragStart={() => handleDragStart()}
              onDragEnd={() => handleDragEnd()}>
                <strong className="title">Sass</strong>
                <span className="percent">75%</span>
            </SkillMorePosedP>
            <SkillMorePosedP
              className="skill webpack-skill"
              scale={0.8}
              onDragStart={() => handleDragStart()}
              onDragEnd={() => handleDragEnd()}>
                <strong className="title">Webpack</strong>
                <span className="percent">80%</span>
            </SkillMorePosedP>
          </animated.article>
        </Fragment>;
      default:
        break;
    }
  };

  return (
    <MorePosedSection
      className={'more-section ' + props.route + '-more'}
      pose={props.isScrollingBottom ? 'show' : 'hide'}
      onMouseMove={({ clientX: x, clientY: y }) => props.isScrollingBottom ? props.set({ xy: props.calc(x, y)}) : false}
      route={props.route}
      delayChildren={props.route === 'home' ? 0 : 250}
      staggerChildren={props.route === 'home' ? 0 : 50}
      duration={500}>
        {handleContent()}
    </MorePosedSection>
  );
};

export default connect(mapStateToProps, null)(More);
