import posed from 'react-pose';

export const LocalesMenuPosedUl = posed.ul({
  show: {
    applyAtStart: {
      display: 'flex',
    },
    y: 50,
    opacity: 1,
    transition: {
      duration: 100
    }
  },
  hide: {
    transition: {
      duration: 100
    },
    y: 0,
    opacity: 0,
    applyAtEnd: {
      display: 'none'
    }
  }
});

export const LocalePosedLi = posed.li({
  show: {
    scale: 2,
    opacity: 1,
  },
  hide: {
    scale: 1,
    opacity: 0
  }
});

export const MorePosedSection = posed.section({
  show: {
    applyAtStart: {
      display: 'flex',
    },
    opacity: 1,
    delayChildren: ({delayChildren}) => delayChildren,
    staggerChildren: ({staggerChildren}) => staggerChildren,
    transition: {
      delay: ({duration}) => duration,
      duration: 500
    }
  },
  hide: {
    transition: {
      duration: 500
    },
    opacity: 0,
    staggerChildren: 50,
    applyAtEnd: {
      display: 'none'
    }
  }
});

export const SkillMorePosedP = posed.p({
  draggable: true,
  show: {
    applyAtStart: {
      display: 'flex',
    },
    opacity: 1,
    scale: ({scale}) => scale,
    boxShadow: '3px 3px 3px rgba(0,0,0,0.50)',
    transition: {
      duration: 250
    }
  },
  hide: {
    opacity: 0,
    scale: 0,
    boxShadow: '0px 0px 0px rgba(0, 0, 0, 0)',
    applyAtEnd: {
      display: 'none'
    }
  }
});

export const AnagramPosedH2 = posed.h2({
  enter: {
    y: 0,
    opacity: 1,
    transition: {
      duration: 50
    }
  },
  exit: {
    y: 10,
    opacity: 0,
    transition: {
      duration: 50
    }
  }
});

export const AboutMorePosedP = posed.p({
  show: {
    applyAtStart: {
      display: 'flex',
    },
    y: 10,
    opacity: 1,
    transition: {
      duration: 1000
    }
  },
  hide: {
    opacity: 0,
    applyAtEnd: {
      display: 'none'
    }
  }
});

export const AboutMorePosedSpan = posed.span({
  show: {
    applyAtStart: {
      display: 'block',
    },
    opacity: 1,
    x: ({positionX}) => positionX,
    transition: {
      duration: 1000
    }
  },
  hide: {
    opacity: 0,
    x: 0,
    applyAtEnd: {
      display: 'none'
    }
  }
});

export const MainPosedSection = posed.section({
  show: {
    scale: 1,
    opacity: 1,
    applyAtStart: {
      display: 'flex'
    },
    transition: {
      duration: 500,
    }
  },
  hide: {
    scale: 0,
    opacity: 0,
    applyAtEnd: {
      display: 'none'
    },
    transition: {
      duration: 500,
    }
  }
});

export const HeaderNavPosedUl = posed.ul({
  show: {
    applyAtStart: {
      display: 'flex'
    },
    opacity: 1,
    staggerChildren: 100
  },
  hide: {
    opacity: 0,
    applyAtEnd: {
      display: 'none'
    },
    staggerChildren: 100
  }
});

export const HeaderNavPosedLi = posed.li({
  show: {
    opacity: 1,
    y: 0
  },
  hide: {
    opacity: 0,
    y: -10
  }
});
