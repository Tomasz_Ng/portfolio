import React, {useState, useEffect} from 'react';
import {Route, Switch} from 'react-router-dom';
import {connect} from 'react-redux';
import {useSpring} from 'react-spring/web.cjs';
import Notification from '../includes/Notification';
import Anagram from '../widgets/Anagram';
import Error from '../views/Error';
import Header from '../includes/Header';
import More from '../includes/More';
import Cue from '../widgets/Cue';
import Tag from '../includes/Tag';
import {
  animateElements,
  handleScroll,
  animateNotification,
  handleSwipe
} from '../../actions';

const mapStateToProps = (state) => {
  return {
    routes: state.root.routes,
    route: state.root.route,
    isScrollingTop: state.root.isScrollingTop,
    isScrollingBottom: state.root.isScrollingBottom,
    isElementsAnimationLoaded: state.root.isElementsAnimationLoaded,
    body: state.root.body,
    isFirstRendering: state.root.isFirstRendering,
    isLocaleSwitch: state.root.isLocaleSwitch
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    animateElements: animateElements(),
    handleScroll: handleScroll({dispatch}),
    animateNotification: animateNotification(),
    handleSwipe: handleSwipe({dispatch})
  };
};

const Router = (props) => {
  const touchStart = {x:0, y:0};
  const calc = (x, y) => [x - window.innerWidth / 2, y - window.innerHeight / 2];
  const trans1 = (x, y) => `translate3d(${x / 50}px,${y / 50}px,0)`;
  const trans2 = (x, y) => `translate3d(${x / 30}px,${y / 30}px,0)`;
  const [positions, set, stop] = useSpring(() => { return { xy: [0, 0], config: { mass: 10, tension: 550, friction: 140 } }});
  const wheelHandler = (event) => {
    return props.handleScroll(event);
  };
  const touchStartHandler = (event) => {
    touchStart.x = event.touches[0].pageX;
    touchStart.y = event.touches[0].pageY;
  };
  const touchMoveHandler = (event) => {
    let offset = {};
    let touch = event.touches[0];
    offset.x = touchStart.x - touch.pageX;
    offset.y = touchStart.y - touch.pageY;

    return props.handleSwipe(event, offset);
  };

  useEffect(() => {
    let header = document.getElementsByTagName('header')[0];
    let notification = document.getElementsByClassName('notification')[0];
    let footer = document.getElementsByTagName('footer')[0];
    let anagram = document.getElementsByClassName('anagram')[0];
    let tag = document.getElementsByClassName('tag')[0];
    let root = document.getElementById('root');

    header.style.opacity = '1';
    footer.style.opacity = '1';
    anagram.style.opacity = '1';
    tag.style.opacity = '1';
    notification.style.display = 'flex';
    notification.style.opacity = '1';

    if (!props.isFirstRendering) {
      props.body.style.backgroundColor = env.PRIMARY_COLOR;
    }

    props.animateNotification(() => {
      notification.style.display = 'none';

      if (!props.isFirstRendering || props.isLocaleSwitch) {
        root.style.boxShadow = 'inset 0px 0px 0px 0px rgba(0,0,0,1)';
      }

      if (props.route.match('^((?!contact|error).)*$')) {
        root.addEventListener('mousewheel', wheelHandler, true);
        root.addEventListener('DOMMouseScroll', wheelHandler, true);
        root.addEventListener('touchstart', touchStartHandler, true);
        root.addEventListener('touchmove', touchMoveHandler, true);
      }
      props.animateElements();
    });

    return () => {
      props.handleScroll(null, 1);
      root.style.boxShadow = 'inset 0px 0px 100px 0px rgba(0,0,0,1)';
      root.removeEventListener('mousewheel', wheelHandler, true);
      root.removeEventListener('DOMMouseScroll', wheelHandler, true);
      root.removeEventListener('touchstart', touchStartHandler, true);
      root.removeEventListener('touchmove', touchMoveHandler, true);
    };
  }, [props.route]);

  return (
    <div className="main">
      <Header/>
      {props.isScrollingBottom && props.isElementsAnimationLoaded && props.route.match('^((?!contact|error).)*$') ? <Cue position={'top'} root={root}/> : null}
      <Tag/>
      <Anagram/>
      <Switch>
        {props.routes.map((route, i) => (
          <Route
            key={i}
            path={route.path}
            exact={route.exact}
            state={route.state}
            children={routeProps => (
              <route.component
                {...routeProps}
                calc={calc}
                trans1={trans1}
                trans2={trans2}
                positions={positions}
                set={set}
                stop={stop}/>
            )}
          />
        ))}
        <Route
          children={(routeProps) => (
            <Error
              {...routeProps}
              calc={calc}
              trans1={trans1}
              trans2={trans2}
              positions={positions}
              set={set}/>
          )}
        />
      </Switch>
      {(props.route.match('^((?!contact|error).)*$')) && <More calc={calc} trans1={trans1} trans2={trans2} positions={positions} set={set} touchMoveHandler={touchMoveHandler} wheelHandler={wheelHandler}/>}
      {props.isScrollingTop && props.isElementsAnimationLoaded && props.route.match('^((?!contact|error).)*$') ? <Cue position={'bottom'} root={root}/> : null}
      <Notification/>
    </div>
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(Router);
