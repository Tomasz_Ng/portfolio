import React from 'react';
import {connect} from 'react-redux';
import {PoseGroup} from 'react-pose';
import {
  AnagramPosedH2,
} from '../core/Pose';

const mapStateToProps = (state) => {
  return {
    anagram: state.root.anagram
  };
};

const Anagram = (props) => {
  return (
    <PoseGroup>
      <AnagramPosedH2
        className="anagram"
        key={props.anagram}>
        {props.anagram}
      </AnagramPosedH2>
    </PoseGroup>
  );
};

export default connect(mapStateToProps, null)(Anagram);
