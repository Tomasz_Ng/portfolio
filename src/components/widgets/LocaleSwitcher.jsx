import React, {useEffect} from 'react';
import {connect} from 'react-redux';
import {
  LocalesMenuPosedUl,
  LocalePosedLi
} from '../core/Pose';
import {
  changeLocale,
  toggleLocaleSwitcher
} from '../../actions';

const mapStateToProps = (state) => {
  return {
    locale: state.intl.locale,
    locales: state.root.locales,
    messages: state.intl.messages,
    isLocaleSwitcherOpen: state.root.isLocaleSwitcherOpen
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    changeLocale: changeLocale({dispatch}),
    toggleLocaleSwitcher: toggleLocaleSwitcher({dispatch})
  };
};

const LocaleSwitcher = (props) => {
  useEffect(() => {
    document.onclick = (e) => {
      e.target.matches && !e.target.matches('.locale-switcher *') && props.isLocaleSwitcherOpen ? props.toggleLocaleSwitcher(false) : false;
    };
  }, [props.isLocaleSwitcherOpen]);

  return (
    <div className="locale-switcher">
      <div
        className="selected-locale"
        title={props.messages['locale_switcher.title']}
        onClick={() => props.toggleLocaleSwitcher(!props.isLocaleSwitcherOpen)}
        style={{
          backgroundImage: `url(${require('../../assets/images/flag_'+ props.locale +'.svg')})`
        }}>
      </div>

      <LocalesMenuPosedUl
        className="locales-menu"
        pose={props.isLocaleSwitcherOpen ? 'show' : 'hide'}
        withParent={false}>
        {props.locales.map((locale, i) => {
          if (locale !== props.locale) {
            return <LocalePosedLi
              key={i}
              className={'locale locale-'+ locale}
              pose={props.isLocaleSwitcherOpen ? 'show' : 'hide'}
              title={props.messages['app.language.'+ locale]}
              onClick={(event) => props.changeLocale(event.currentTarget.dataset.value)}
              data-value={locale}
              style={{
                backgroundImage: `url(${require('../../assets/images/flag_'+ locale +'.svg')})`
              }}>
            </LocalePosedLi>;
          }
        })}
      </LocalesMenuPosedUl>
    </div>
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(LocaleSwitcher);
