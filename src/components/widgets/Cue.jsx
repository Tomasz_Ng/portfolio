import React from 'react';
import {connect} from 'react-redux';
import {
  handleScroll
} from '../../actions';

const mapStateToProps = (state) => {
  return {
    messages: state.intl.messages,
    isFirefox: state.root.isFirefox,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    handleScroll: handleScroll({dispatch})
  };
};

const Cue = (props) => {
  const simulateScroll = () => {
    if (props.position === 'top') {
      props.handleScroll(null, 1);
    } else {
      props.handleScroll(null, -1);
    }
  };

  return (
    <div className={'cue ' + props.position + ' icon icon-arrow'} onClick={() => simulateScroll()} title={props.position === 'top' ? props.messages['cue.title.top'] : props.messages['cue.title.bottom']}></div>
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(Cue);
