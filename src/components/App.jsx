require('../config/lib/react-intl');

import React, {Fragment} from 'react';
import Layout from './Layout';

const App = () => {
  return (
    <Fragment>
      <Layout/>
    </Fragment>
  );
};

export default App;
