import React from 'react';
import {connect} from 'react-redux';
import {animated} from 'react-spring/web.cjs';
import Nav from '../includes/Nav';
import {
  MainPosedSection
} from '../core/Pose';
import {
  handleAnagram
} from '../../actions';

const mapStateToProps = (state) => {
  return {
    route: state.root.route,
    messages: state.intl.messages,
    isScrollingBottom: state.root.isScrollingBottom
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    handleAnagram: handleAnagram({dispatch})
  };
};


const Skills = (props) => {
  return (
    <MainPosedSection className="main-section" pose={props.isScrollingBottom ? 'hide' : 'show'}>
      <Nav
        trans1={props.trans1}
        positions={props.positions}
        history={props.history}
        set={props.set}
        calc={props.calc}/>

      <animated.article className="page-content" style={{transform: props.positions.xy.to(props.trans2)}}>
        <aside
          className="element element-4"
          data-x="37"
          data-y="-15"
          data-scale="3"
          data-rotate-min="10"
          data-rotate-max="-2"
          data-width="5"
          data-height="5"
          data-duration="750"
          data-padding="5"
          data-border-radius="50%">

          <div
            className="sub-element"
            data-duration="750">
            <h2 className="title">{props.messages['skills.title_front'].split('').map((letter, i) => {
              return <span
                key={i}
                className={'letter'}
                style={letter === ' ' ? {width: '3px'} : null}
                onClick={(e) => props.handleAnagram(e)}>
                {letter}
              </span>;
            })}</h2>
            <span className="percent percent-front"></span>
          </div>
        </aside>

        <aside
          className="element element-5"
          data-x="60"
          data-y="-5"
          data-scale="3"
          data-rotate-min="10"
          data-rotate-max="-2"
          data-width="4"
          data-height="4"
          data-duration="750"
          data-padding="5"
          data-border-radius="50%">

          <div
            className="sub-element"
            data-duration="750">
            <h2 className="title">{props.messages['skills.title_back'].split('').map((letter, i) => {
              return <span
                key={i}
                className={'letter'}
                style={letter === ' ' ? {width: '3px'} : null}
                onClick={(e) => props.handleAnagram(e)}>
                {letter}
              </span>;
            })}</h2>
            <span className="percent percent-back"></span>
          </div>
        </aside>
      </animated.article>
    </MainPosedSection>
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(Skills);
