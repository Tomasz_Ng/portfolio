import React from 'react';
import {connect} from 'react-redux';
import {animated} from 'react-spring/web.cjs';
import Nav from '../includes/Nav';
import {
  MainPosedSection
} from '../core/Pose';
import {
  handleAnagram
} from '../../actions';

const mapStateToProps = (state) => {
  return {
    route: state.root.route,
    messages: state.intl.messages,
    isMobilePortrait: state.root.isMobilePortrait,
    isScrollingBottom: state.root.isScrollingBottom
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    handleAnagram: handleAnagram({dispatch})
  };
};

const About = (props) => {
  return (
    <MainPosedSection className="main-section" pose={props.isScrollingBottom ? 'hide' : 'show'}>
      <Nav
        trans1={props.trans1}
        positions={props.positions}
        history={props.history}
        set={props.set}
        calc={props.calc}/>

      <animated.article className="page-content" style={{transform: props.positions.xy.to(props.trans2)}}>
        <aside
          className="element element-4"
          data-x={props.isMobilePortrait ? '24' : '30'}
          data-y={props.isMobilePortrait ? '-10' : '-13'}
          data-scale="1.4"
          data-rotate-min="5"
          data-rotate-max="-5"
          data-width={props.isMobilePortrait ? '22.5' : '26.5'}
          data-height={props.isMobilePortrait ? '24.5' : '26.5'}
          data-duration="750"
          data-padding="5"
          data-border-radius="50%">

          <div
            className="sub-element"
            data-duration="750">
            <h2 className="question">{props.messages['about.question_1'].split('').map((letter, i) => {
              return <span
                key={i}
                className={'letter'}
                style={letter === ' ' ? {width: '3px'} : null}
                onClick={(e) => props.handleAnagram(e)}>
                {letter}
              </span>;
            })}</h2>
            <p className="answer">{props.messages['about.answer_1']}</p><br/>
            <h2 className="question">{props.messages['about.question_2'].split('').map((letter, i) => {
              return <span
                key={i}
                className={'letter'}
                style={letter === ' ' ? {width: '3px'} : null}
                onClick={(e) => props.handleAnagram(e)}>
                {letter}
              </span>;
            })}</h2>
            <p className="answer" dangerouslySetInnerHTML={{__html: props.messages['about.answer_2']}}></p><br/>
            <h2 className="question">{props.messages['about.question_3'].split('').map((letter, i) => {
              return <span
                key={i}
                className={'letter'}
                style={letter === ' ' ? {width: '3px'} : null}
                onClick={(e) => props.handleAnagram(e)}>
                {letter}
              </span>;
            })}</h2>
            <p className="answer">{props.messages['about.answer_3']}</p>
          </div>
        </aside>
      </animated.article>
    </MainPosedSection>
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(About);
