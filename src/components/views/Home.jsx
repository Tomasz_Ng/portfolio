import React from 'react';
import {connect} from 'react-redux';
import {animated} from 'react-spring/web.cjs';
import Nav from '../includes/Nav';
import {
  MainPosedSection
} from '../core/Pose';
import {
  handleAnagram
} from '../../actions';

const mapStateToProps = (state) => {
  return {
    route: state.root.route,
    content_author: state.root.content_author,
    content_specialty: state.root.content_specialty,
    content_job: state.root.content_job,
    isScrollingBottom: state.root.isScrollingBottom,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    handleAnagram: handleAnagram({dispatch})
  };
};

const Home = (props) => {
  return (
    <MainPosedSection className="main-section" pose={props.isScrollingBottom ? 'hide' : 'show'}>
      <Nav
        trans1={props.trans1}
        positions={props.positions}
        history={props.history}
        set={props.set}
        calc={props.calc}
        stop={props.stop}/>

      <animated.article className="page-content" style={{transform: props.positions.xy.to(props.trans2)}}>
        <aside
          className="element element-4"
          data-x="42"
          data-y="-30"
          data-scale="3"
          data-rotate-min="5"
          data-rotate-max="5"
          data-width="8"
          data-height="1.82"
          data-font-size="1.1"
          data-duration="650"
          data-padding="1">

          <h2
            className="sub-element author"
            data-duration="650">
            {props.content_author.split('').map((letter, i) => {
              return <span
                key={i}
                className={'letter'}
                style={letter === ' ' ? {width: '3px'} : null}
                onClick={(e) => props.handleAnagram(e)}>
                {letter}
              </span>;
            })}
          </h2>
        </aside>

        <aside
          className="element element-5"
          data-x="43"
          data-y="-16"
          data-scale="3.8"
          data-rotate-min="10"
          data-rotate-max="10"
          data-width="7.5"
          data-height="1.8"
          data-font-size="1.3"
          data-duration="750"
          data-padding="1">

          <h2
            className="sub-element specialty"
            data-duration="750">
            {props.content_specialty.split('').map((letter, i) => {
              return <span
                key={i}
                className={'letter'}
                onClick={(e) => props.handleAnagram(e)}>
                {letter}
              </span>;
            })}
          </h2>
        </aside>

        <aside
          className="element element-6"
          data-x="42"
          data-y="-2"
          data-scale="5.3"
          data-rotate-min="5"
          data-rotate-max="5"
          data-width="8"
          data-height="1.7"
          data-font-size="1.1"
          data-duration="850"
          data-padding="1">

          <h2
            className="sub-element job"
            data-duration="850">
            {props.content_job.split('').map((letter, i) => {
              return <span
                key={i}
                className={'letter'}
                style={letter === ' ' ? {width: '3px'} : null}
                onClick={(e) => props.handleAnagram(e)}>
                {letter}
              </span>;
            })}
          </h2>
        </aside>
      </animated.article>
    </MainPosedSection>
  )
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
