import React from 'react';
import {connect} from 'react-redux';
import {animated} from 'react-spring/web.cjs';
import Nav from '../includes/Nav';
import {
  handleAnagram
} from '../../actions';

const mapStateToProps = (state) => {
  return {
    route: state.root.route,
    locale: state.intl.locale,
    messages: state.intl.messages,
    isMobilePortrait: state.root.isMobilePortrait
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    handleAnagram: handleAnagram({dispatch})
  };
};


const Contact = (props) => {
  return (
    <section className="main-section">
      <Nav
        trans1={props.trans1}
        positions={props.positions}
        history={props.history}
        set={props.set}
        calc={props.calc}/>

      <animated.article className="page-content" style={{transform: props.positions.xy.to(props.trans2)}}>
        <aside
          className="element element-4"
          data-x={props.isMobilePortrait ? '29' : '35'}
          data-y={props.isMobilePortrait ? '-10' : '-14'}
          data-scale="1.4"
          data-rotate-min="5"
          data-rotate-max="0"
          data-width="20"
          data-height="18"
          data-border-radius="50%"
          data-duration="750"
          data-padding="5">

          <div
            className="sub-element"
            data-duration="650">
            <h2 className="question">{props.messages['contact.discuss'].split('').map((letter, i) => {
              return <span
                key={i}
                className={'letter'}
                style={letter === ' ' ? {width: '3px'} : null}
                onClick={(e) => props.handleAnagram(e)}>
                {i === 14 || i === (props.locale === 'en' ? 21 : 23) ? <br/> : letter}
              </span>;
            })}</h2>
            <p className="answer">{props.messages['contact.find_me']}</p><br/>
            <ul>
              <li><a className="icon icon-linkedin" href="https://www.linkedin.com/in/tomasz-ngondo/" title="Linkedin" target={'_blank'} rel="noopener"></a></li>
              <li><a className="icon icon-twitter" href="https://twitter.com/NgondoTomasz" title="Twitter" target={'_blank'} rel="noopener"></a></li>
              <li><a className="icon icon-bitbucket" href="https://bitbucket.org/Tomasz_Ng/scalenge/src/master/" title="Bitbucket" target={'_blank'} rel="noopener"></a></li>
            </ul>
          </div>
        </aside>
      </animated.article>
    </section>
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(Contact);
