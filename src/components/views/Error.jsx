import React, {useEffect} from 'react';
import {connect} from 'react-redux';
import {handleAnagram} from '../../actions';
import {animated} from 'react-spring/web.cjs';
import Nav from '../includes/Nav';

const mapStateToProps = (state) => {
  return {
    messages: state.intl.messages,
    route: state.root.route,
    content_error_title: state.root.content_error_title,
    isMobilePortrait: state.root.isMobilePortrait
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    handleAnagram: handleAnagram({dispatch})
  };
};


const Error = (props) => {
  return (
    <section className="main-section">
      <Nav
        trans1={props.trans1}
        positions={props.positions}
        history={props.history}
        set={props.set}
        calc={props.calc}/>

      <animated.article className="page-content" style={{transform: props.positions.xy.to(props.trans2)}}>
        <aside
          className="element element-1"
          data-x={props.isMobilePortrait ? '40' : '43'}
          data-y="-20"
          data-scale="3"
          data-rotate-min="10"
          data-rotate-max="5"
          data-width="9.5"
          data-height="2.1"
          data-font-size="1.8"
          data-duration="650"
          data-padding="1">

          <h2
            className="sub-element error-title"
            data-duration="650">
            {props.content_error_title.split('').map((letter, i) => {
              return <span
                key={i}
                className={'letter'}
                style={letter === ' ' ? {width: '3px'} : null}
                onClick={(e) => props.handleAnagram(e)}>
                {letter}
              </span>;
            })}
          </h2>
        </aside>

        <aside
          className="element element-2"
          data-x={props.isMobilePortrait ? '25' : '35'}
          data-y="-10"
          data-scale="1.4"
          data-rotate-min="8"
          data-rotate-max="2"
          data-width="24"
          data-height="15"
          data-border-radius="50%"
          data-duration="750"
          data-padding="5">

          <p
            className="sub-element"
            data-duration="1500"
            dangerouslySetInnerHTML={{__html: props.messages['error.content']}}>
          </p>
        </aside>
      </animated.article>
    </section>
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(Error);
