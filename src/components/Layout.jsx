import React, {Fragment} from 'react';
import Router from './core/Router';
import Footer from './includes/Footer';

const Layout = () => {
  return (
    <Fragment>
      <Router/>
      <Footer/>
    </Fragment>
  );
};

export default Layout;
